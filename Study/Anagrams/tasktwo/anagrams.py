def reverse_word(word):
    non_alpha = {i: c for (i, c) in enumerate(word) if not c.isalpha()}
    source_sequence = [c for c in word if c.isalpha()]
    source_sequence.reverse()
    for i, c in non_alpha.items():
        source_sequence.insert(i, c)
    return ''.join(source_sequence)


def reverse_text(text):
    return ' '.join(map(reverse_word, text.split()))


if __name__ == "__main__":
    cases = [
        ("abcd efgh", "dcba hgfe"),
        ("a1bcd efg!h", "d1cba hgf!e"),
        ("ab2,c4d ef!gh$", "dc2,b4a hg!fe$")
    ]
    for plain_text, reversed_text in cases:
        assert reverse_text(plain_text) == reversed_text
