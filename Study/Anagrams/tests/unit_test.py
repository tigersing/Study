from unittest import TestCase, main

from tasktwo.anagrams import reverse_text


class ReverseText(TestCase):
    def test_reverse_text(self):
        self.assertEqual(reverse_text("abcd"), "dcba")
        self.assertNotEqual(reverse_text("abcd"), "dcbb")
        self.assertEqual(reverse_text("ya&h!"), "ha&y!")
        self.assertEqual(reverse_text("-+!?/"), "-+!?/")


if __name__ == "__main__":
    main()
