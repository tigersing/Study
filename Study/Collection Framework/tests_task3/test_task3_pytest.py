import pytest

from collection_framework.collection_framework import count


def test_count():
    assert count("abcd") == 4
    assert count("ya&h!") == 5


if __name__ == '__main__':
    pytest.main()
