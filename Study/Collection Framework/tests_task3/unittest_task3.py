from unittest import TestCase, main
from collection_framework.collection_framework import count


class Count(TestCase):
    def test_count(self):
        self.assertEqual(count("abbccc"), 1)
        self.assertEqual(count("abcd"), 4)
        self.assertNotEqual(count("abcd"), 5)
        self.assertEqual(count("ya&h!"),5)
        self.assertEqual(count("-+!?/"), 5)


if __name__ == "__main__":
    main()
